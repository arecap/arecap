#Agile Ready Enterprise Content Application Program#


Lead by [agilemanifesto] ARECAP is an open source community in job of software development guide by discipline, professionalism, passion and maturity.

The open community program goals is to follow up software craftsmanship development by publishing everything concerning the related work and other useful articles and documentations. 

ARECAP open source technology stack enables cloud platform development for Enterprise and offers a lot of many more related ready Enterprise frameworks.

ARECAP open source for platform designed libraries, frameworks, documentations and repositories should be according to this Development Guide and implemented according [Product Management] Guide. 

This Development Guide should cover all the documentation needed as much as Agile principle of working software over comprehensive documentation according with Product Management Guide. The full set of the ARECAP development process will be public and glad to meet the changes of the professionals co-works.   

##Agile Ready Enterprise Content Application Platform

The program working software is currently published on [GitLab] public platform.

----------

##Program Content
* software for platform development
*  guides lead by working examples
* by doing training programs 
* stepping stone documentations

----------

##Platform open source related technologies

[GitLab Community Edition], [Apache], [maven], [Spring], [Vaadin], [jsonwebtoken.io], [Johann Burkard], [sfl4j],[logback]

----------

#Table of content
[TOC]


[agilemanifesto]:http://agilemanifesto.org
[Product Management]:https://kanban-chi.appspot.com/dashboard/5107088418406400/d-5107088418406400
[GitLab]:https://gitlab.com/arecap
[GitLab Community Edition]:https://docs.gitlab.com/ce/README.html
[Apache]:https://www.apache.org/
[maven]:https://maven.apache.org/
[Spring]:https://spring.io/
[Vaadin]:http://vaadin.com/
[jsonwebtoken.io]:https://www.jsonwebtoken.io/
[Johann Burkard]:http://johannburkard.de/software/uuid/
[sfl4j]:https://www.slf4j.org/
[logback]:https://logback.qos.ch/